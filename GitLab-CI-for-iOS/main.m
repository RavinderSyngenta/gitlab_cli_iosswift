//
//  main.m
//  GitLab-CI-for-iOS
//
//  Created by Mac Mini One on 15/05/19.
//  Copyright © 2019 Syngenta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
