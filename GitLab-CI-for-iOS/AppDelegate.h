//
//  AppDelegate.h
//  GitLab-CI-for-iOS
//
//  Created by Mac Mini One on 15/05/19.
//  Copyright © 2019 Syngenta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

